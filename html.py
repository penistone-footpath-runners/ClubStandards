#!/usr/bin/env python3
MFHEAD = f'<head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR Club Standards</title><link rel="stylesheet" href="../style.css"></head><body><div class="maindiv"><img class="logo" src="../logo.png" alt="Penistone Footpath Runners & AC"><div class="title"><h1>Penistone Footpath Runners &amp; AC</h1><h2>Club Standards</h2></div><table align="center" style="border-collapse:separate;width:500px;table-layout:fixed"><th><a href="index.html">Runners</a></th><th><a href="standardslist.html">Standards</a></th><th><a href="http://pfrac.co.uk/background/club-standards-for-road-performances/" target="_blank">Information</a></th></table>'

NUMBERS = "0123456789"
STANDARDS = [None, "1st Class", "2nd Class", "3rd Class", "4th Class"]
COLOURS = ("#FFFFFF", "#ffD700", "#C0C0C0", "#A87900", "#4682B4")


def create_html_table(
    data,
    headingrow=0,
    columnalign=None,
    highlight=False,
    sticky=True,
    colcols=None,
    linkables=[],
    headingcolumn=None,
    colrows=None,
):
    """Creates an HTML table of some data"""
    if columnalign is None:
        columnalign = ["text-align:left"]
    while len(columnalign) < len(data[0]):
        columnalign.append("text-align:center")

    # Determine how many padding (wierd) spaces are needed to fit all
    # of the numbers
    digitsneeded = []
    for i in range(0, len(data[0]), 1):
        digitsneeded.append(
            max(
                len(str(round(x[i]))) if type(x[i]) in (float, int) else 0 for x in data
            )
        )
    table = '<table style="table-layout:fixed">'
    for i, row in enumerate(data):
        table += "<tr>"
        for j, cell in enumerate(row):
            text = str(cell)

            # Make the class row better than just a number
            if type(cell) == int:
                text = STANDARDS[cell]

            # Create boldness
            if highlight:
                if row in highlight:
                    text = f"<strong>{text}</strong>"

            # Make it a link if needed
            if text in linkables:
                text = f'<a href="{text}.html">{text}</a>'

            # Create if heading
            if i == headingrow or j == headingcolumn:
                if sticky:
                    table += f'<th class="sticky" style="{columnalign[j]}">{text}</th>'
                else:
                    table += f'<th style="{columnalign[j]}">{text}</th>'

            # Create standard cell
            elif colcols:
                table += f'<td style="{columnalign[j]};background-color:{colcols[j]}">{text}</td>'
            elif colrows:
                table += f'<td style="{columnalign[j]};background-color:{colrows[i]}">{text}</td>'
            else:
                table += f'<td style="{columnalign[j]}">{text}</td>'

        table += "</tr>"

    table += "</table>"
    return table


def create_html_page(
    pagename, data, titles, headingrow=0, columnalign=None, highlight=False
):
    """Create an HTML page for some data"""
    if titles[0][1]:
        head = f"{MFHEAD}<h2>{pagename} – {STANDARDS[titles[0][1]]}</h2>"
    else:
        head = f"{MFHEAD}<h2>{pagename}</h2>"

    head += f'<table><th><a href="../Runner Pages/{pagename}.html">Click to see results for all of {pagename.split(" ")[0]}\'s competitions.</a></th></table>'
    colcols = [[COLOURS[0 if x == "" else x] for x in i[4]] for i in data]

    mts = [
        f'<br><h3 style="color:{COLOURS[titles[x][1]]}">{titles[x][0]}</h3>{create_html_table(data[x], headingrow, columnalign, highlight, colcols=colcols[x])}'
        for x in range(len(data))
    ]
    if data[0] == [
        ("5K", "10K", "10M", "HM", "20M", "Mar"),
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
        ["", "", "", "", "", ""],
    ]:
        crosssite = f"<em>{pagename} has done no races that qualify them for a standard in their current age group.</em>"
    else:
        crosssite = mts[0][4:]
    mts = "".join(mts)
    return [f"{head}{mts}", crosssite]


def create_html_list(
    pagename, data, linkables, headingrow=0, columnalign=None, highlight=False
):
    """Create an HTML page for some data"""
    head = f"{MFHEAD}<h2>List of Runners</h2><p>Note: List shows standard achieved in current age group. Click on any name to see details of races and past achievements.</p>"
    mts = create_html_table(
        data, headingrow, columnalign, highlight, linkables=linkables
    )
    return f"{head}{mts}"
