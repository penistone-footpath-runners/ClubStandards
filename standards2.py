#!/usr/bin/env python3
MODE = "linear"

DISTANCES = {
    "5K": 5,
    "10K": 10,
    "10M": 10 * 1.609344,
    "HM": 42.195 / 2,
    "20M": 20 * 1.609344,
    "Mar": 42.195,
}

CATEGORIES = {
    "Senior": 25,
    "40–44": 40,
    "45–49": 45,
    "50–54": 50,
    "55–59": 55,
    "60–64": 60,
    "65–69": 65,
    "70–74": 70,
    "75–79": 75,
    "80+": 80,
}

PERCENTS = [x / 100 for x in (75, 65, 55, 45)]


class Point(tuple):
    """A point, with x and y values"""

    def __new__(self, arg):
        return super().__new__(Point, arg)

    def __init__(self, arg):
        super().__init__()
        assert len(self) == 2

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    def __repr__(self):
        return f"Point({super().__repr__()})"


def linear(points, x):
    a, b = points
    return a.y * (x - b.x) / (a.x - b.x) + b.y * (x - a.x) / (b.x - a.x)


def cubic(points, x):
    a, b, c, d = points
    return (
        (a.y * (x - b.x) * (x - c.x) * (x - d.x))
        / ((a.x - b.x) * (a.x - c.x) * (a.x - d.x))
        + (b.y * (x - a.x) * (x - c.x) * (x - d.x))
        / ((b.x - a.x) * (b.x - c.x) * (b.x - d.x))
        + (c.y * (x - a.x) * (x - b.x) * (x - d.x))
        / ((c.x - a.x) * (c.x - b.x) * (c.x - d.x))
        + (d.y * (x - a.x) * (x - b.x) * (x - c.x))
        / ((d.x - a.x) * (d.x - b.x) * (d.x - c.x))
    )


def floor(value, interval=1):
    return interval * (value // interval)


def roundtime(time):
    if time < 1800:
        return floor(time, 15)
    if time < 3600:
        return floor(time, 30)
    return floor(time, 60)


with open("factors.csv", "r") as f:
    factors = [row.split(",") for row in f.read().split("\n")]

while [""] in factors:
    factors.remove([""])

# Remove the useless distance and formatted time rows
factors.remove(factors[0])
factors.remove(factors[2])

halfway = len(factors[0]) // 2

factordistances = [float(x) for x in factors.pop(0)[1:halfway]]
openseconds = factors.pop(0)
openseconds.remove("OC sec")
openseconds.remove("OC sec")
openseconds = [int(x) for x in openseconds]
openseconds = {"F": openseconds[: halfway - 1], "M": openseconds[halfway - 1 :]}

# Split the table into genders
genderfactors = {"F": [], "M": []}
for row in factors:
    genderfactors["F"].append(row[:halfway])
    genderfactors["M"].append(row[halfway:])
factors = genderfactors

# Turn the tables into age: times dictionaries and convert to numbers
for g in factors:
    factors[g] = {int(row[0]): [float(x) for x in row[1:]] for row in factors[g]}

# Make the new table
t = {}
for g in factors:
    gd = {}
    for c in CATEGORIES:
        cd = {}
        for d in DISTANCES:
            if DISTANCES[d] in factordistances:
                index = factordistances.index(DISTANCES[d])
                hundred = openseconds[g][index] / factors[g][CATEGORIES[c]][index]
            else:
                for i in range(len(factordistances)):
                    if factordistances[i] > DISTANCES[d]:
                        biggerindex = i
                        break
                else:
                    biggerindex = len(factordistances)
                eitherside = {"linear": 1, "cubic": 2}[MODE]
                while i + eitherside >= len(factordistances):
                    i -= 1
                interpolatefunc = {"linear": linear, "cubic": cubic}[MODE]
                surrounders = list(range(i - eitherside, i + eitherside))
                points = [
                    Point((factordistances[i], openseconds[g][i])) for i in surrounders
                ]
                realos = interpolatefunc(points, DISTANCES[d])

                points = [
                    Point((factordistances[i], factors[g][CATEGORIES[c]][i]))
                    for i in surrounders
                ]
                realfactor = interpolatefunc(points, DISTANCES[d])
                hundred = realos / realfactor

            cd[d] = [int(roundtime(hundred / p)) for p in PERCENTS]
        gd[c] = cd
    t[g] = gd

newstandards = t


def Standard(gender, category, distance, time):
    vals = newstandards[gender][category][distance]
    # To test for errors in making the data; can probably be deleted later
    assert sorted(vals) == vals

    return sorted([time] + vals).index(time) + 1
