#!/usr/bin/env python3
import re
import os
import xml.etree.ElementTree as ET
import zlib
import datetime
import urllib
import requests
import argparse

import additionals
import removables
import standards2
import html

try:
    import cPickle as pickle
except ImportError:
    import pickle

# Settings
YEAR = 2020
with open("runner_names.csv", "r") as f:
    runner_names = f.read().split("\n")

while "" in runner_names:
    runner_names.remove("")

runner_names = [row.strip(",").split(",")[1:] for row in runner_names]
runner_names = filter(lambda row: row[0] != "0", runner_names)
runner_names = {row[0]: [None] if row[1] == "None" else row[1:] for row in runner_names}

with open("ages.txt", "r") as f:
    AGES = tuple(f.read().strip("\n").split("\n"))

DATE = datetime.datetime.now().date().isoformat()
PATHTOHTML = "ClubStandards/"
CATEGORIES = tuple(standards2.CATEGORIES.keys())

LEVELS = ("1st Class", "2nd Class", "3rd Class", "4th Class")
MONTHS = (
    None,
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
)
SEXES = {"F": "Female", "M": "Male"}
BAR = 50
GOODCOLUMNS = (0, 1, 4, 10, 11)
VALIDEVENTS = tuple(standards2.DISTANCES.keys())
ALLOWEDEVENTS = {"5000": "5K", "10000": "10K"}
# Find the list of parkruns that are allowed
with open("parkruns.txt", "r") as f:
    VALIDPARKRUNS = set(f.read().split("\n"))
VALIDPARKRUNS.discard("")

MAINURL = "https://www.thepowerof10.info/athletes/"
CLUBURL = f"{MAINURL}athleteslookup.aspx?club=Penistone"
RUNNERURL = f"{MAINURL}profile.aspx?athleteid="
CLUBTABLESTART = (
    '<table cellspacing="0" cellpadding="3" id="cphBody_dgAthletes" width="100%">'
)
RUNTABLESTART = '<table width="100%" cellspacing="0" cellpadding="2" rules="none" class="alternatingrowspanel">'
BADROWCOL = '<tr style="background-color:DarkGray;">'

names = {}


class RunnerNotFound(Exception):
    pass


class Runner:
    def __init__(self, name, gender, data, date_of_birth, datejoined, ignoreafter=None):
        """Create a new runner"""
        self.name = name
        self.gender = gender
        try:
            data = data + additionals.races[self.name]
        except KeyError:
            pass
        try:
            removes = removables.races[self.name]
            self.data = []
            for race in data:
                if [race[2], race[3]] not in removes:
                    self.data.append(race.copy())
        except KeyError:
            self.data = [x.copy() for x in data]

        self.date_of_birth = date_of_birth
        self.datejoined = datejoined
        self.category = age_to_category(self.age_at(DATE), self.gender)
        # Convert all of the dates and add an age (category) column
        for run in self.data:
            if "-" not in run[3]:
                run[3] = uka_to_iso(run[3])
            run.append(age_to_category(self.age_at(run[3]), gender))

        if ignoreafter is None:
            ignoreafter = DATE

        # Destroy the runs before membership by creating a dummy for
        # sorting into it. Also use another dummy to ignore later
        # races.
        earlydummy = [None, None, None, datejoined, None]
        latedummy = [None, None, None, ignoreafter, None]
        self.data.insert(0, earlydummy)
        self.data.append(latedummy)
        self.data.sort(key=lambda x: x[3])
        self.data = self.data[
            self.data.index(earlydummy) + 1 : self.data.index(latedummy)
        ]
        # Used for abusing the program to find lists of things or stuff
        self.alldata = [[self.name] + x for x in self.data]

        # Find only the best performances
        newdata = []
        bests = {}
        for run in self.data:
            # Some rows say this for some reason
            if run[1] == "TBC":
                continue
            info = (run[0], run[4])  # Distance and age cat
            try:
                if info not in bests or bests[info] > time_to_secs(run[1]):
                    bests[(run[0], run[4])] = time_to_secs(run[1])
                    newdata.append(run)
            except ValueError:
                if GETRECORDS:
                    continue
                raise ValueError
        self.data = newdata

        # Calculate the standard for each run
        if GETRECORDS:
            return
        for run in self.data:
            run.append(
                standards2.Standard(self.gender, run[4], run[0], time_to_secs(run[1]))
            )
            # To show rare 5th(!) standards
            # if standards2.Standard(self.gender, run[4], run[0], time_to_secs(run[1])) == 5:
            # print(f'Runner: {name}\nData: {run}')

    def age_at(self, date):
        """Return the runner's age on a particular date"""
        approx = int(date.split("-")[0]) - int(self.date_of_birth.split("-")[0])
        # Determine if their birthday has been reached this year
        sorty = [date[5:], self.date_of_birth[5:]]
        if sorted(sorty) == sorty:
            return approx - 1
        return approx

    def calculate_standards(self):
        """Return the standards that the runner has achieved"""
        standards = {}
        for run in self.data:
            # 5th class is not a thing, so it is excluded
            if run[5] < 5:
                if run[4] not in standards:
                    standards[run[4]] = {}
                standards[run[4]][run[0]] = run
        try:
            self.grade = achieved_grade(
                [x[5] for x in standards[self.category].values()]
            )
        except KeyError:
            self.grade = 0
        return standards


def achieved_grade(grades):
    """Find the grade that the runner has achieved from all of their
    races"""
    for grade in range(1, 5, 1):
        if sum(grades.count(i) for i in range(1, grade + 1, 1)) >= 3:
            return grade
    return 0


def age_to_category(age, gender):
    """Convert from age to age category"""
    if age >= 80:
        return "80+"
    return AGES[age]


def uka_to_iso(date):
    """Convert from the date format on UKA to ISO format"""
    day, month, year = date.split(" ")
    month = str(MONTHS.index(month))
    year = 2000 + int(year)
    if year > YEAR:
        year -= 100
    if len(month) == 1:
        month = "0" + month
    if len(day) == 1:
        day = "0" + day

    return f"{year}-{month}-{day}"


def time_to_secs(time):
    """Convert a time string to seconds"""
    val = sum(
        (0 if x == "00" else float(x.lstrip("0"))) * 60 ** i
        for i, x in enumerate(time.strip().split(":")[::-1])
    )
    if val == int(val):
        return int(val)
    return val


def format_time(secs):
    """Format a time nicely."""
    if secs < 6000:
        return f"{secs // 60}:{str(secs % 60).zfill(2)}"
    hours = secs // 3600
    secs = secs % 3600
    return f"{hours}:{str(secs // 60).zfill(2)}:{str(secs % 60).zfill(2)}"


def GetHTML(url):
    """Get the HTML code, as a string, from a website"""
    request = urllib.request.Request(url, data=None)
    r = urllib.request.urlopen(request)
    if not isinstance(r, str):
        r2 = r.read()
        r.close()
        r = r2
    try:
        n = r.decode()
    except UnicodeDecodeError:
        print("ERROR: " + url)
        n = ""
    return n


def parse_page(page, runnerid):
    """Extract a table from the UKA page"""
    # Reduce down to the table
    name = page[page.find("<h2>") + 4 :]
    name = name[: name.find("</h2>")]
    names[runnerid] = name.lstrip()
    page = page[page.find(RUNTABLESTART) + len(RUNTABLESTART) :]
    page = f'<table>{page[:page.find("</table")]}</table>'

    # Delete all of the <a> misusing year-headings
    while BADROWCOL in page:
        loc = page.find(BADROWCOL)
        restoftable = page[loc:]
        page = page.replace(restoftable[: restoftable.find("</tr>") + 5], "")

    # The space breaks parser for some reason
    page = page.replace("nowrap ", "")

    # Amperands break parser
    page = page.replace("&", "&amp;")

    # Parse the table and convert it into a nicer format
    root = ET.fromstring(page)
    table = [[y.text for y in x] for x in root]

    # Remove heading rows
    table = list(filter(lambda x: x != [None] * 12, table))
    table = [[x[y] for y in GOODCOLUMNS] for x in table]

    # Make the time the minimum of the time rows (for chip time)
    for row in table:
        try:
            row.remove(None)
        except ValueError:
            pass
        if len(row) == 4:
            continue
        time1 = time_to_secs(row[1])
        if time_to_secs(row[2]) > time1:
            row.remove(row[2])
        else:
            row.remove(row[1])
    return table


def reduce_to_parkrun(text):
    return text[: text.find("parkrun") - 1]


def make_pages(dl):
    if dl:
        # Get the page with the list of Penistone runners
        listpage = GetHTML(CLUBURL)

        # Trim it down to just the table
        listpage = listpage[listpage.find(CLUBTABLESTART) :]
        listpage = listpage[: listpage.find("</table")]

        # Find the runners using a regex
        athletes = set(re.findall("athleteid=[0-9]+", listpage))
        # Get rid of the 'athleteid=' part
        athletes = [re.search("[0-9]+", athlete).group(0) for athlete in athletes]

        # Download all runner files
        print(f'Downloading runner files...\n|{" " * BAR}|\n|', end="")
        pages = {}

        for i, athlete in enumerate(athletes):
            pages[athlete] = GetHTML(RUNNERURL + athlete)
            # Draw bar animation
            if round(BAR * (i + 1) / len(athletes)) > round(BAR * i / len(athletes)):
                print("=", end="", flush=True)
        print("|\nDownloaded!")

        with open("savedpages.dat", "wb") as f:
            f.write(zlib.compress(pickle.dumps(pages)))
    else:
        with open("savedpages.dat", "rb") as f:
            pages = pickle.loads(zlib.decompress(f.read()))

    for runner in pages:
        page = parse_page(pages[runner], runner)
        # Create a runner from filtering the table that parse_page produces,
        # getting rid of events which do not count
        pages[runner] = list(
            filter(
                lambda x: x[0] in VALIDEVENTS + tuple(ALLOWEDEVENTS)
                or (x[0] == "parkrun" and reduce_to_parkrun(x[2]) in VALIDPARKRUNS)
                or GETRECORDS,
                page,
            )
        )
        for run in pages[runner]:
            if run[0] in ALLOWEDEVENTS:
                run[0] = ALLOWEDEVENTS[run[0]]
            elif run[0] == "parkrun":
                run[0] = "5K"

    # Copy the CSS and logo files to their new location
    if not os.path.exists(PATHTOHTML):
        os.makedirs(PATHTOHTML)

    # Create the list of runner objects
    runners = []
    named_pages = {}
    errors = False
    for runner in pages:
        try:
            theirdata = runner_names[runner]
        except KeyError:
            print(f"{runner} not found.")
            errors = True
            continue
        if theirdata[0] is not None:
            r = Runner(
                theirdata[0], theirdata[1], pages[runner], theirdata[2], theirdata[3]
            )
            named_pages[theirdata[0]] = pages[runner]
            runners.append(r)
    if errors:
        raise RunnerNotFound("Runners have not been found in the names file.")

    # Get some records data
    if GETRECORDS:
        records = {}
        flat = [
            [r.name, r.gender] + x
            for r in runners
            for x in r.data
            if r.age_at(DATE) >= 20 and "parkrun" not in x[2]
        ]
        for g in SEXES:
            for c in CATEGORIES:
                records[(g, c)] = {}
                for row in flat:
                    if row[6] == c and row[1] == g:
                        if row[1] not in records[(g, c)] or time_to_secs(
                            row[3]
                        ) < time_to_secs(records[(g, c)][row[2]][3]):
                            records[(g, c)][row[2]] = row

        print(records)

    # Open the cross-results site individual pages
    with open("../RunnerPages/pages.txt", "r") as f:
        runner_pages = eval(f.read())

    linkedrunners = []
    for runner in runners:
        # Figure out their standards
        stds = runner.calculate_standards()
        # Do not create a page for runners with no standards
        if not stds:
            continue
        linkedrunners.append(runner.name)
        # This variable stores the actual tables to be created
        alltables = []
        # This stores the little titles which go above the tables,
        # and their qualifications
        titles = []
        cats = list(stds.keys())
        if runner.category not in cats:
            cats.append(runner.category)
            stds[runner.category] = {}
        cats.sort(key=(AGES + tuple(["80+"])).index, reverse=True)
        for category in cats:
            # Create the table template
            table = [VALIDEVENTS, [], [], [], []]
            for dist in VALIDEVENTS:
                if dist in stds[category]:
                    # Add the column values
                    table[1].append(stds[category][dist][2])
                    table[2].append(stds[category][dist][3])
                    table[3].append(stds[category][dist][1])
                    table[4].append(stds[category][dist][5])

                else:
                    # Empty column for bad ones
                    table[1].append("")
                    table[2].append("")
                    table[3].append("")
                    table[4].append("")
            # Add this table to the list
            alltables.append(table)
            # Add the title, with its colour
            titles.append(
                (category, achieved_grade([x[5] for x in stds[category].values()]))
            )

        # Create and write the file
        table, crosssite = html.create_html_page(
            runner.name, alltables, titles, columnalign=["text-align:center"]
        )
        with open(f"{PATHTOHTML}{runner.name}.html", "w") as f:
            f.write(table)
        # Cross-site
        if runner.name not in runner_pages:
            runner_pages[runner.name] = {}
        runner_pages[runner.name]["Club Standards"] = crosssite

    # Cross-site
    with open("../RunnerPages/pages.txt", "w") as f:
        f.write(str(runner_pages))

    # Make the list
    table = [
        [
            i.name,
            i.category,
            i.grade
            if i.grade != 0 or i.category not in i.calculate_standards()
            else f"None ({len(i.calculate_standards()[i.category].keys())}/3)",
        ]
        for i in runners
        if i.name in linkedrunners
    ]

    table = sorted(table, key=lambda x: (x[0].split(" ")[1], x))
    with open(f"{PATHTOHTML}/index.html", "w") as f:
        f.write(
            html.create_html_list(
                "List of Runners",
                [["Name", "Category", "Current Class"]] + table,
                linkedrunners,
            )
        )

    # Make the big list
    for year in (str(x) for x in range(2010, YEAR + 1)):
        fakerunners = [
            Runner(
                r.name,
                r.gender,
                named_pages[r.name],
                r.date_of_birth,
                r.datejoined,
                year + "-12-31",
            )
            for r in runners
        ]
        table = [
            [i.name]
            + [
                achieved_grade([d[5] for d in i.calculate_standards()[x].values()])
                if x in i.calculate_standards()
                else ""
                for x in CATEGORIES
            ]
            if i.name in linkedrunners
            else None
            for i in fakerunners
        ]
        while None in table:
            table.remove(None)
        table = sorted(table, key=lambda x: x[0].split(" ")[-1])
        with open(f"{PATHTOHTML}/index{year}.html", "w") as f:
            f.write(
                html.create_html_list(
                    "List of Runners",
                    [tuple(["Name"]) + CATEGORIES] + table,
                    linkedrunners,
                )
            )

    # Create the standards page
    page = html.MFHEAD + "<h2>Current Standards</h2>"
    for category in CATEGORIES:
        for gender in SEXES:
            table = [[""] + list(VALIDEVENTS)] + [
                [LEVELS[level]]
                + [
                    format_time(
                        standards2.newstandards[gender][category][distance][level]
                    )
                    for distance in VALIDEVENTS
                ]
                for level in range(4)
            ]
            page += f"<h3>{SEXES[gender]} {category}<h3>"
            page += html.create_html_table(
                table, sticky=False, headingcolumn=0, colrows=html.COLOURS
            )

    with open(f"{PATHTOHTML}/standardslist.html", "w") as f:
        f.write(page)

    # For analysing how many people do each; not entirely necessary
    vals = {x: 0 for x in VALIDEVENTS}
    for runner in runners:
        events = set()
        for run in runner.data:
            events.add(run[0])
        for event in events:
            vals[event] += 1
    runnervals = vals

    infos = []
    for runner in runners:
        infos += runner.alldata
    vals = {x: 0 for x in VALIDEVENTS}
    for run in infos:
        vals[run[1]] += 1

    racevals = vals

    text = ["Distance,Performances,Runners"]
    for event in VALIDEVENTS:
        text.append(f"{event},{racevals[event]},{runnervals[event]}")

    text = "\n".join(text)

    with open("AmountDone.csv", "w") as f:
        f.write(text)

    # # Find 5Ks—temporary
    # runs = []
    # for runner in runners:
    # runs += list(filter(lambda x: x[1] == '5K', runner.alldata))

    # print(str(runs).replace('\', \'', ',').replace('\'], [\'', '\n'))


def main():
    parser = argparse.ArgumentParser(
        description="Generate Club Standards HTML from the powerof10 website."
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="Whether to download new data from the website",
    )
    parser.add_argument(
        "--getrecords",
        action="store_true",
        help="Get data intended for the Club Records (doesn't generate normal standards properly)",
    )
    args = parser.parse_args()
    global GETRECORDS
    GETRECORDS = args.getrecords
    make_pages(args.download)


if __name__ == "__main__":
    main()
